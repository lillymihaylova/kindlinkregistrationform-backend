CREATE TABLE IF NOT EXISTS list
(
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(150) NOT NULL,
    registration_num varchar(30) NOT NULL,
    date_of_registration date NOT NULL,
    owner varchar(150) NOT NULL,
    address varchar(200) NOT NULL,
    post_code varchar(10) NOT NULL,
    sphere_of_activity varchar(200) NOT NULL,
    description_of_activity text NOT NULL,
    PRIMARY KEY (id)
);	
	 
