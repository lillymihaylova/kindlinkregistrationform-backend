package com.kindlink.app.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.kindlink.app.models.Organization;

@Transactional
public interface OrganizationRepository extends CrudRepository<Organization, Long> {
 


	
	 
}
