package com.kindlink.app.models;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;




@Entity
@Table(name = "list")
public class Organization {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	
	@Column(nullable=false,name="id")
	private long id;

	@Column(nullable=false, name ="name")
	private String organizationName;

	@Column(nullable=false,name="registration_num")
	private String registrationNumber;

	@Column(nullable=false,name="date_of_registration")
	private String dateOfRegistration;

	@Column(nullable=false,name="owner")
	private String owner;

	@Column(nullable=false,name ="address")
	private String address;

	@Column(nullable=false,name="post_code")
	private String postCode;

	@Column(nullable=false,name="sphere_of_activity")
	private String sphereOfActivity;

	@Column(nullable=false,name="description_of_activity")
	private String descriptionOfActivity;

	public Organization(){

	}



	public Organization(String organizationName, String registrationNumber, String dateOfRegistration, String owner,
			String address, String postCode, String sphereOfActivity, String descriptionOfActivity) {
		
		
		this.organizationName = organizationName;
		this.registrationNumber = registrationNumber;
		this.dateOfRegistration = dateOfRegistration;
		this.owner = owner;
		this.address = address;
		this.postCode = postCode;
		this.sphereOfActivity = sphereOfActivity;
		this.descriptionOfActivity = descriptionOfActivity;
	}



	public long getId() {
		return id;
	}



	public void setId(long id) {
		this.id = id;
	}



	public String getOrganizationName() {
		return organizationName;
	}



	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}



	public String getRegistrationNumber() {
		return registrationNumber;
	}



	public void setRegistrationNumber(String registrationNumber) {
		this.registrationNumber = registrationNumber;
	}



	public String getDateOfRegistration() {
		return dateOfRegistration;
	}



	public void setDateOfRegistration(String dateOfRegistration) {
		this.dateOfRegistration = dateOfRegistration;
	}



	public String getOwner() {
		return owner;
	}



	public void setOwner(String owner) {
		this.owner = owner;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getPostCode() {
		return postCode;
	}



	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}



	public String getSphereOfActivity() {
		return sphereOfActivity;
	}



	public void setSphereOfActivity(String sphereOfActivity) {
		this.sphereOfActivity = sphereOfActivity;
	}



	public String getDescriptionOfActivity() {
		return descriptionOfActivity;
	}



	public void setDescriptionOfActivity(String descriptionOfActivity) {
		this.descriptionOfActivity = descriptionOfActivity;
	}



	


	

}
