package com.kindlink.app;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;




@SpringBootApplication
public class KindLinkRegistrationFormDemoApplication {

	public static void main(String[] args) {
			SpringApplication.run(KindLinkRegistrationFormDemoApplication.class, args);
	}
}
