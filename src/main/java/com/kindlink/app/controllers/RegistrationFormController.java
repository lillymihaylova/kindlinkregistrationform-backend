package com.kindlink.app.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.kindlink.app.models.Organization;
import com.kindlink.app.repositories.OrganizationRepository;

@CrossOrigin(origins="*")
@RestController
public class RegistrationFormController {


	@RequestMapping(value = "/app/components/**", method = RequestMethod.GET)
	public String registerForm(Model model) {
		model.addAttribute("organization", new Organization());
		return "index.html";
	}  

	@RequestMapping(value="/registration-form",method = RequestMethod.POST)
	public ResponseEntity<Organization> addOrganization(@RequestBody Organization organization) {
		
		create(organization);
	return new ResponseEntity<Organization>(organization, HttpStatus.OK); 

	}  
	
	@ModelAttribute("/organizations")
	public List<Organization> all() {
		return (List<Organization>) organizationRepository.findAll();
	}


	@RequestMapping("/create")
	@ResponseBody
	public String create(Organization organization) {
		try {
			organizationRepository.save(organization);
		} catch (Exception ex) {
			return "Error creating the user: " + ex.toString();
		}
		return "You successfully created a registration!";
	}  
	

	@RequestMapping(value = "/organizations-list", method = RequestMethod.GET)
	public @ResponseBody List<Organization> allOrganizations(Model model) {
		return (List<Organization>) this.organizationRepository.findAll();
	}

	@Autowired
	private  OrganizationRepository organizationRepository;
}

